<?php

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

function formulaires_share_charger_dist() {
	$valeurs = [
		'titre'     => _request('titre'),
		'nom_site'  => '',
		'url_site'  => _request('url_site'),
		'texte'     => _request('extrait'),
		'logo'      => _request('logo'),
		'id_parent' => '1',
		'ps'        => ''
	];

	return $valeurs;
}

function formulaires_share_verifier_dist() {
	$erreurs      = [];
	$obligatoires = [
		'titre',
		'id_parent',
	];
	foreach ($obligatoires as $obligatoire) {
		if (!_request($obligatoire)) {
			$erreurs[$obligatoire] = _T('info_obligatoire');
		}
	}

	return $erreurs;
}

function formulaires_share_traiter_dist() {
	$id_auteur = (int)$GLOBALS['visiteur_session']['id_auteur'];

	$titre        = _request('titre');
	$nom_site     = _request('nom_site');
	$id_rubrique  = intval(_request('id_parent'));
	$url_site     = _request('url_site');
	$texte        = _request('texte');
	$ps           = _request('ps');
	$ajouter_logo = _request('ajouter_logo') == 'on' ? true : false;
	$logo         = _request('logo');
	$logo 		  = explode('?', $logo)[0];

	// créer un article dans la bonne rubrique
	include_spip('action/editer_objet');
	$id_article = objet_inserer('article', $id_rubrique);

	// statut de publication souhaité
	$statut = lire_config('curator/statut_souhaite', 'publie');

	if ($id_article) {
		// insérer les valeurs saisies
		$valeurs = [
			'titre'    => $titre,
			'texte'    => $texte,
			'nom_site' => $nom_site,
			'url_site' => $url_site,
			'date'     => date('Y-m-d H:i:s'),
			'ps'       => $ps,
			'statut'   => $statut,
		];
		objet_modifier('article', $id_article, $valeurs);

		// associer l'auteur
		objet_associer(
			['auteur' => $id_auteur],
			['article' => $id_article]
		);

		// ajout les mots clés
		if ($etiquettes = _request('tags')) {
			include_spip('inc/tag-machine');
			$groupe_mots = sql_getfetsel('titre', 'spip_groupes_mots', 'id_groupe=' . lire_config('curator/groupe_mots'));
			if (!$groupe_mots) {
				$groupe_mots = 'Tags';
			}
			ajouter_mots($etiquettes, $id_article, $groupe_mots, 'articles', 'id_article');
		}

		// ajouter le logo
		if ($ajouter_logo && $logo) {
			include_spip('inc/distant');
			$logo_file = copie_locale($logo, 'force');
			include_spip('action/editer_logo');
			logo_modifier('article', $id_article, 'on', $logo_file);
			spip_unlink($logo_file);
		}

		// fermer la popup
		echo '<script language="JavaScript">self.close();</script>';
		exit;
	}
	else {
		return ['message_erreur' => _L("Erreur lors de la création de l'article")];
	}
}
