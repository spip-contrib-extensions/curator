<?php

/**
 * Options du plugin curator au chargement
 *
 * @plugin     curator
 * @copyright  2014
 * @author     ydikoi
 * @licence    GNU/GPL
 * @package    SPIP\Curator\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

// annuler le comportement ajax sur le selecteur rubriques (ne semble plus nécessaire)
// defined('_SPIP_SELECT_RUBRIQUES')
// 	or define('_SPIP_SELECT_RUBRIQUES', 10000);
