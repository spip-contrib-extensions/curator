<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_logo' => 'Ajouter le logo ?',

	// B
	'bouton_effacer' => 'Effacer les valeurs',

	// C
	'curator_titre' => 'curator',
	'configurer_rubrique' => 'Ajouter les articles dans la rubrique',
	'configurer_rubrique_explication' => 'Si vous ne choisissez aucune rubrique (ou secteur), elles seront toutes accessibles par défaut',
	'configurer_statut_souhaite' => 'Statut de publication de l’article',
	'configurer_statut_souhaite_explication' => 'Cette préférence s’applique dans la mesure des droits dont  l’auteur dispose.',
	'configurer_groupe_mots' => 'Groupe de mots clés',
	'configurer_groupe_mots_explication' => 'Par défaut, un groupe de mots clés "Tags" sera créé',
	'coller_adresse' => 'Coller l’adresse de l’image à utiliser comme logo',
	
	// L
	'logo' => 'Logo',
	
	// T
	'titre_page_configurer_curator' => 'Configurer'
	
);
